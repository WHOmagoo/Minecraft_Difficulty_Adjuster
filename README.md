# Minecraft Difficulty Adjuster

Allows you to make Minecraft easier for users when difficulty is set to Hard

The datapack works by automatically giving the following potion effects to players that have been marked for easy mode
 - Resistance 2
 - Health Boost 1

To add users to easy mode, add them to the da_easy_mode scorebaord with a value of 1 or greater.

```scoreboard players set WHOmagoo da_easy_mode 1```

The effects won't get activated until the player dies, or drinks a bucket of milk

#Handles milk
effect give @e[scores={da_drank_milk=1..,da_easy_mode=1..}] minecraft:health_boost 999999 0 true
effect give @e[scores={da_drank_milk=1..,da_easy_mode=1..}] minecraft:resistance 999999 1 true
scoreboard players reset @e da_drank_milk

#Handles deaths
effect give @e[scores={da_death_tracker=1..,da_easy_mode=1..}] minecraft:health_boost 999999 0 true
effect give @e[scores={da_death_tracker=1..,da_easy_mode=1..}] minecraft:resistance 999999 1 true
scoreboard players reset @e da_death_tracker

#effect give @a[scores={da_drank_milk=1,da_easy_mode=1..}] minecraft:instant_health 1 55

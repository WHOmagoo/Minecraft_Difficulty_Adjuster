#Setup scoreboards
scoreboard objectives add da_drank_milk minecraft.used:minecraft.milk_bucket
scoreboard objectives add da_easy_mode dummy
scoreboard objectives add da_death_tracker deathCount

#Add players to easy scoreboard
scoreboard players set Tasha_Ved da_easy_mode 1
scoreboard players set Stasia da_easy_mode 1
scoreboard players set WHOmagoo2 da_easy_mode 1
scoreboard players set MandaPropaganda da_easy_mode 1
scoreboard players set Isabelly da_easy_mode 1
